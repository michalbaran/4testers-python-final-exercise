class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.products = []
        self.purchased = False

    #metoda dodająca produkt do listy produktów (self.products), używamy funkcji append
    def add_product(self, product):
        self.products.append(product)

    #metoda zwracająca całkowitą cenę za produkty w zamówieniu
    def get_total_price(self):
        total_price = 0
        for product in self.products:
            total_price += product.get_price()
        return total_price

    #metoda zwracająca całkowitą ilość produktów w zamówieniu
    def get_total_quantity_of_products(self):
        total_quantity_of_products = 0
        for product in self.products:
            total_quantity_of_products += product.quantity
        return total_quantity_of_products


    #metoda zmieniająca status zakupu (self.purchased) na zrealizowany
    def purchase(self):
        self.purchased = True


class Product:
    def __init__(self, name, unit_price, quantity=1):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    #metoda zwracająca cenę, czyli cęnę za 1 sztukę razy ilość sztuk danego produktu
    def get_price(self):
        return self.unit_price * self.quantity

